# Calendario Hackeditorial 2022

[Descarga el PDF para impresión](https://gitlab.com/programando-libreros/zines/calendario-hackeeditorial-2022/-/raw/main/pdfs/calendario-hackeditorial-2022.pdf)

![](galeria/f01.jpg)
![](galeria/f02.jpg)
![](galeria/f03.jpg)
![](galeria/f04.jpg)
![](galeria/f05.jpg)

## Producción

Para producir el PDF solo ejecuta `./hacer_pdf`:

![](galeria/produccion.gif)

### Requisitos

* ImageMagick
* Ruby
* PDFtk
* Gema RMagick
* [img2booklet](https://gitlab.com/-/snippets/2260086)

## Licencia

Este texto está bajo [Licencia Editorial Abierta y Libre (LEAL)](https://programando.li/bres). Con LEAL eres libre de usar, copiar, reeditar, modificar, distribuir o comercializar bajo las siguientes condiciones:

* Los productos derivados o modificados han de heredar algún tipo de LEAL.
* Los archivos editables y finales habrán de ser de acceso público.
* El contenido no puede implicar difamación, explotación o vigilancia.
